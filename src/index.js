import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chart from "chart.js";
import Dropdown from "react-dropdown";

class DrawItem extends React.Component {
  render() {
    return (
      <span className={this.props.className}>
          {this.props.label}: {this.props.value}
      </span>
    );
  }
}


function getRandomDateArray(numItems) {
  // Create random array of objects (with date)
  let data = [];
  let baseTime = new Date('2018-05-01T00:00:00').getTime();
  let dayMs = 24 * 60 * 60 * 1000;
  for(var i = 0; i < numItems; i++) {
    data.push({
      time: new Date(baseTime + i * dayMs),
      value: Math.round(20 + 80 * Math.random())
    });
  }
  return data;
}

// LineChart
class LineChart extends React.Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.data.map(d => d.timestamp);
    this.myChart.data.datasets[0].data = this.props.data.map(d => d.speed);
    this.myChart.update();
  }

  componentDidMount() {
    this.myChart = new Chart(this.canvasRef.current, {
      type: 'line',
      options: {
			  maintainAspectRatio: false,
        scales: {
          xAxes: [
            {
              type: 'time',
              time: {
                unit: 'week'
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                min: 0
              }
            }
          ]
        }
      },
      data: {
        labels: this.props.data.map(d => d.timestamp),
        datasets: [{
          label: this.props.title,
          data: this.props.data.map(d => d.speed),
          fill: 'none',
          backgroundColor: this.props.color,
          pointRadius: 2,
          borderColor: this.props.color,
          borderWidth: 1,
          lineTension: 0
        }]
      }
    });
  }

  render() {
    return <canvas ref={this.canvasRef} />;
  }
}


class BarChart extends React.Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.data.map(d => d.label);
    this.myChart.data.datasets[0].data = this.props.data.map(d => d.sum);
    this.myChart.update();
  }

  componentDidMount() {
    this.myChart = new Chart(this.canvasRef.current, {
      type: 'bar',
      options: {
          maintainAspectRatio: false,
        scales: {
          yAxes: [
            {
              ticks: {
                min: 0,
                max: 10
              }
            }
          ]
        }
      },
      data: {
        labels: this.props.data.map(d => d.label),
        datasets: [{
          label: this.props.title,
          data: this.props.data.map(d => d.sum),
          backgroundColor: this.props.color
        }]
      }
    });
  }

  render() {
    return (
        <canvas ref={this.canvasRef} />
    );
  }
}


class DoughnutChart extends React.Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.data.map(d => d.label);
    this.myChart.data.datasets[0].data = this.props.data.map(d => d.sum);
    this.myChart.update();
  }

  componentDidMount() {
    this.myChart = new Chart(this.canvasRef.current, {
      type: 'doughnut',
      options: {
        maintainAspectRatio: false },

      data: {
        labels: this.props.data.map(d => d.label),
        datasets: [{
          data: this.props.data.map(d => d.sum),
          backgroundColor: this.props.colors }] } });




  }


  render() {
    return React.createElement("canvas", { ref: this.canvasRef });
  }
}


class Main extends React.Component {
  constructor(props) {
    super(props);
    this._onSelect = this._onSelect.bind(this);
    this.state = {
      komunikat_wyboru: "Wybierz zapis przejazdu",
      wybor: 0,
      Kierowcy: [{
        label: 'Loading...',
        sum: 0
      }],
      Trasy: [{
              car: 'Loading...',
              distance: 'Loading...',
              driver: 'Loading...',
              endLatitude: 'Loading...',
              endLongitude: 'Loading...',
              id: 'Loading...',
              longitude: 'Loading...',
              maxAltitude: 'Loading...',
              minAltitude: 'Loading...',
              routeTarget: 'Loading...',
              routesList: [{
                altitude: 'Loading...',
                id: 'Loading...',
                latitude: 'Loading...',
                locationId: 'Loading...',
                longitude: 'Loading...',
                speed: '0',
                timestamp: "2019-12-13T16:55:03.846"
              }]
            }],
      Miejsce: [{
        label: 'Loading...',
        sum: 0
      }],
      Odcinki: ['Loading...'],
      Samochody: [{
        label: 'Loading...',
        sum: 0
      }],
      Dropdown: ['Loading...'],
    };
  }

  sumUpAttribute(slownik) {
    var output;
    output = [];
    for (var key in slownik) {
          if (slownik.hasOwnProperty(key)) {
            output.push(slownik[key]);
          }
        }
    return output;
  }

  componentDidMount() {
    fetch('https://biedauber.firebaseio.com/biedauber.json')
      .then(response => response.json())
      .then(data => {
        var i;
        var trasy;
        trasy = [];
        var kierowcy;
        kierowcy = {};
        var samochody;
        samochody = {};
        var miejsca;
        miejsca = {};
        var dropdown;
        dropdown = [];
        var indeks;
        indeks = 0;
        for (i = 0; i < data.Trasy.length; i++) {
          if (data.Trasy[i] != null) {
            dropdown.push({
                label: data.Trasy[i].driver + ' ' +
                    data.Trasy[i].car + ' ' + data.Trasy[i].routeTarget + ' ' + data.Trasy[i].id,
                value: indeks
            });
            indeks += 1;
            if (kierowcy[data.Trasy[i].driver] == null) {
              kierowcy[data.Trasy[i].driver] = {
                label: data.Trasy[i].driver,
                sum: 0
              };
            }
            kierowcy[data.Trasy[i].driver]["sum"] += 1;
            if (samochody[data.Trasy[i].car] == null) {
              samochody[data.Trasy[i].car] = {
                label: data.Trasy[i].car,
                sum: 0
              };
            }
            samochody[data.Trasy[i].car]["sum"] += 1;
            if (miejsca[data.Trasy[i].routeTarget] == null) {
              miejsca[data.Trasy[i].routeTarget] = {
                label: data.Trasy[i].routeTarget,
                sum: 0
              };
            }
            miejsca[data.Trasy[i].routeTarget]["sum"] += 1;
            trasy.push(
                data.Trasy[i]
            )
          }
        }
        console.log(dropdown);
        var drivers;
        drivers = this.sumUpAttribute(kierowcy);
        var cars;
        cars = this.sumUpAttribute(samochody);
        var places;
        places = this.sumUpAttribute(miejsca);
        this.setState({
	      Kierowcy: drivers,
	      Trasy: trasy,
          // Trasy: getRandomDateArray(150),
	      Miejsce: places,
	      Samochody: cars,
          Dropdown: dropdown
       });
       console.log(trasy);
      });
  }

  _onSelect(option) {
      console.log(this.state);
      this.setState({
          wybor: option['value'],
          komunikat_wyboru: option['label']
      });
  }

  render() {
  var koloryDoughnuta;
  koloryDoughnuta = ["#a8e0ff", "#8ee3f5", "#70cad1", "#3e517a", "#b08ea2", "#BBB6DF"]
    return (
      <div className="aplikacja">
        <div>
            <h1>BiedaUber</h1>
        </div>
        <div className="Dropdown">
            <Dropdown
                options={this.state.Dropdown}
                onChange={this._onSelect}
                value=""
                placeholder={this.state.komunikat_wyboru}
                arrowClosed={<span className="Dropdown-arrow" />}
                arrowOpen={<span className="Dropdown-arrow" />}
            />
        </div>
          <div className="list chart-wrapper">
              <h2>Szczegóły wybranego przejazdu:</h2>
              <DrawItem
                    className="listItem"
                    label="Kierowca"
                    value={this.state.Trasy[this.state.wybor].driver}
              />
              <DrawItem
                    className="listItem"
                    label="Samochóð"
                    value={this.state.Trasy[this.state.wybor].car}
              />
              <DrawItem
                    className="listItem"
                    label="Kierowca"
                    value={this.state.Trasy[this.state.wybor].driver}
              />
              <DrawItem
                    className="listItem"
                    label="Pozycja startowa"
                    value={this.state.Trasy[this.state.wybor].startLongitude + ' ' + this.state.Trasy[this.state.wybor].startLatitude}
              />
              <DrawItem
                    className="listItem"
                    label="Pozycja końcowa"
                    value={this.state.Trasy[this.state.wybor].endLongitude + ' ' + this.state.Trasy[this.state.wybor].endLatitude}
              />
              <DrawItem
                    className="listItem"
                    label="Przebyty dystans"
                    value={this.state.Trasy[this.state.wybor].distance}
              />
              <DrawItem
                    className="listItem"
                    label="Czas"
                    value={this.state.Trasy[this.state.wybor].time}
              />
          </div>
          <div className="main chart-wrapper">
              <LineChart
                    data={this.state.Trasy[this.state.wybor].routesList}
                    title="Wykres prędkości w czasie"
                    color="#70CAD1"
              />
          </div>
          <div>
              <h2 className="dddd">Podsumowanie wszystkich przejazdów:</h2>
          <div className="sub chart-wrapper">
              <BarChart
                    data={this.state.Miejsce}
                    title="Kierunki podróży"
                    color="#B08EA2"
              />
          </div>
          <div className="sub chart-wrapper">
              <BarChart
                data={this.state.Samochody}
                title="Udział samochodów we wszystkich przejazdach"
                color="#70CAD1"
              />
          </div>
        <div className="sub chart-wrapper">
              <DoughnutChart
                data={this.state.Kierowcy}
                title="Udział kierowców"
                colors={koloryDoughnuta}
              />
          </div>
          </div>
       {/*   <div className="game-info">*/}
       {/*     <DrawList*/}
       {/*         name='Lista kierowców'*/}
       {/*         className='listaKierowcow'*/}
	   {/*         list={this.state.Kierowcy}*/}
	   {/*     />*/}
	   {/*     <DrawList*/}
       {/*         name='Lista miejsc'*/}
       {/*         className='listaMiejsc'*/}
	   {/*         list={this.state.Miejsce}*/}
	   {/*     />*/}
	   {/*     <DrawList*/}
       {/*         name='Lista samochodów'*/}
       {/*         className='listaSamochodow'*/}
	   {/*         list={this.state.Samochody}*/}
	   {/*     />*/}
	   {/*</div>*/}
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);

